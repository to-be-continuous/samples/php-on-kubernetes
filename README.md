# PHP on Kubernetes project sample

This project sample shows the usage of _to be continuous_:

* PHP
* SonarQube ([sonarcloud.io](https://sonarcloud.io/))
* Docker
* Kubernetes ([Flexible Engine - Cloud Container Engine](https://cloud.orange-business.com/en/offers/infrastructure-iaas/public-cloud/features/cloud-container-engine/))

The project builds and deploys the [Measurement Conversion](https://code-projects.org/measurement-conversion-in-php-with-source-code/)
project sample.

## PHP template features

This project uses the following features from the GitLab CI PHP template:

* Uses [PHPUnit](https://docs.phpunit.de/) tests with code coverage,
* [PHP_CodeSniffer](https://github.com/squizlabs/PHP_CodeSniffer) analysis.

## SonarQube template features

This project uses the following features from the SonarQube template:

* Defines the `SONAR_HOST_URL` (SonarQube server host),
* Defines `organization` & `projectKey` from [sonarcloud.io](https://sonarcloud.io/) in `sonar-project.properties`,
* Defines :lock: `$SONAR_TOKEN` as secret CI/CD variable,
* Uses the `sonar-project.properties` to specify project specific configuration:
    * source and test folders,
    * reuses code coverage report from [PHPUnit](https://docs.phpunit.de/) test job,
    * reuses JUnit test report from [PHPUnit](https://docs.phpunit.de/) test job (:warning: doesn't work due to
      invalid JUnit format not supported by SonarQube).

## Docker template features

This project builds and _pushes_ a Docker image embedding the built Maven application.
For this, a Docker registry is required.
As a matter of fact there are two options (this project implements the first one).

### Option 1: use the GitLab registry

This is the easiest as the Docker template is preconfigured to work this way.

Extra requirements:

1. make sure the Kubernetes cluster has (network) access to the GitLab registry (true in our case through the internet),
2. create a Kubernetes secret with GitLab registry credentials to `docker pull` images:
    ```bash
    kubectl create secret docker-registry gitlab-registry-credentials --docker-server=registry.gitlab.com --docker-username=token --docker-password=$SOME_GITLAB_TOKEN
    ```
3. use those credentials when using images from GitLab, with the `imagePullSecrets` field.

### Option 2: use the Flexible Engine registry

This project could easily use the Kubernetes Cluster's Docker registry (`registry.eu-west-0.prod-cloud-ocb.orange-business.com`).
For this, the following would be required:

1. make sure the GitLab runners have (network) access to the external registry,
2. Override default `$DOCKER_SNAPSHOT_IMAGE` and `$DOCKER_RELEASE_IMAGE` complying to specific [Flexible Engine's policy](https://docs.prod-cloud-ocb.orange-business.com/usermanual/swr/swr_01_0011.html):
    ```yaml
    DOCKER_SNAPSHOT_IMAGE: registry.eu-west-0.prod-cloud-ocb.orange-business.com/to-be-continuous/burger-maker/snapshot:$CI_COMMIT_REF_SLUG
    DOCKER_RELEASE_IMAGE: registry.eu-west-0.prod-cloud-ocb.orange-business.com/to-be-continuous/burger-maker:$CI_COMMIT_REF_NAME
    ```
3. Define :lock: `$DOCKER_REGISTRY_USER` and :lock: `$DOCKER_REGISTRY_PASSWORD` as secret project variables, obtained according [Flexible Engine documentation](https://docs.prod-cloud-ocb.orange-business.com/usermanual/swr/swr_01_1000.html).

## Kubernetes template features

This project uses the following features from the Kubernetes template:

* Overrides the default `kubectl` version to match CCE version by declaring `$K8S_KUBECTL_IMAGE` in the `.gitlab-ci.yml` file,
* Defines mandatory `$K8S_URL`,
* Defines :lock: `$K8S_CA_CERT` and :lock: `$K8S_TOKEN` (related to a generated [CI/CD service account](https://www.auroria.io/kubernetes-ci-cd-service-account-setup/)) as secret variables,
* Enables review environments by declaring the `$K8S_REVIEW_SPACE` in the project variables,
* Enables staging environment by declaring the `$K8S_STAGING_SPACE` in the project variables,
* Enables production environment by declaring the `$K8S_PROD_SPACE` in the project variables.

The Kubernetes template also implements [environments integration](https://docs.gitlab.com/ee/ci/environments/) in GitLab:

* deployment environment integrated in merge requests,
* review environment cleanup support (manually or when the related feature branch is deleted).

### implementation details

In order to perform Kubernetes deployments, this project implements:

* `deployment.yml` template: instantiates all required Kubernetes objects,
* `k8s-cleanup.sh` script that implements applications cleanup (`review` environments only).

All those scripts and descriptors make use of variables dynamically evaluated and exposed by the Kubernetes template:

* `${environment_name}`: the application target name to use in this environment (ex: `maven-on-kubernetes-review-fix-bug-12`)
* `${docker_image_digest}`: the docker image (with digest) that was just built in the upstream pipeline and that is being deployed
* `${stage}`: the CI job stage (equals `$CI_JOB_STAGE`)
* `${hostname}`: the environment hostname, extracted from `$CI_ENVIRONMENT_URL` (declared as [`environment:url`](https://docs.gitlab.com/ee/ci/yaml/#environmenturl) in the `.gitlab-ci.yml` file)
